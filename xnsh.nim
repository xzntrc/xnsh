import std/os
var
    cmd: string
    cwd = getCurrentDir()
    hostos: string
    configpath: string
    configExists: bool
# Checking OS
when defined windows:
  hostos = "windows"
  configpath = getHomeDir() & "xnsh"
elif defined linux:
  hostos = "linux"
  configpath = getHomeDir() & ".config/xnsh/"
else:
  hostos = "other"

configExists = existsOrCreateDir(configpath)

if configExists == false:
    echo "Config could not be found."
    echo "Creating at " & configpath & "..."

proc cmdHelp() =
    echo "Test Help Message"

proc n(cmd: string) =
    echo "xsh: The command '", $cmd, "' could not be found"

proc cmdParsing(){.noconv.} =
    while true:
        stdout.write "\e[1;33m⚡\e[0m\e[1;32m", $cwd, "\e[0m "
        cmd = readLine(stdin)

        if cmd == "help":
            cmdHelp()
        else:
            n(cmd)
setControlCHook(cmdParsing)
cmdParsing()
